﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    StartRoom,
    EndRoom, 
    TreasureRoom,
    Hall,
    Room,
    EndCap,
}

public class Tile : MonoBehaviour
{
    //the name of this type of tile
    public string Name;

    //the type of tile this is
    public TileType Type;
    //the size of the Tile, used for collision detection on level generation

    public List<Connector> Connectors;
    //list of items contained in the tile

    public List<ItemMount> ItemMounts;

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Ray r = new Ray(transform.position, transform.up);
        Gizmos.DrawRay(r);

        Gizmos.color = Color.red;
        r = new Ray(transform.position, -transform.right);
        Gizmos.DrawRay(r);
    }

}


