﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConnectorType
{
    smallDoor,
    largeDoor,
}

public class Connector : MonoBehaviour
{
    public ConnectorType Type;
    
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Ray r = new Ray(transform.position, transform.up);
        Gizmos.DrawRay(r);

        Gizmos.color = Color.red;
        r = new Ray(transform.position, -transform.right);
        Gizmos.DrawRay(r);
    }

    //Not impl.. future notes
    private void LoadFromFile()
    {
        //this.gameObject.tag = "";
    }

}
