﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Buildstate
{
    Start,
    PlaceStartRoom,
    PlaceTiles,
    PlaceEndRoom,
    PlaceCaps,
    FillItems,
    CheckLevel,
    Done
}

public class LevelCreator : MonoBehaviour
{
    //prefabs that we can load and use to produce a level
    public Tile StartingTile;
    public Tile EndTile;
    public List<Tile> TilePrefabs;

    public int Target_Number_Rooms;
    public int Failures_Allowed;

    //lists containing current tiles / connectors in the level 
    List<Connector> OpenConnectors;
    List<Tile> PlacedTiles;
    List<ItemMount> OpenItemMounts;

    private Connector targetConnector = null;
    private Connector processingConnector = null;
    private Tile tileInProcess = null;
    private int failures = 0;
    private int iteration = 0;

    private Buildstate buildState = Buildstate.Start;

	void Awake ()
    {
        //Initialize all the lists and load any settings / prefabs from disk
        Initialize();

        //Generate the level
        PreGenerateLevel();
    }


    //TODO: make state machine to flip through processes
    void FixedUpdate()
    {
        switch(buildState)
        {
            case Buildstate.Done: //done goes first for quick out at the end of the build.
                {
                    break;
                }
            case Buildstate.Start:
                {
                    buildState = Buildstate.PlaceStartRoom;
                    break;
                }
            case Buildstate.PlaceStartRoom:
                {
                    //Place a starting room at origin
                    PlaceStartingRoom(StartingTile);
                    break;
                }
            case Buildstate.PlaceTiles:
                {
                    ProcessLevelStep();
                    break;
                }
            case Buildstate.PlaceEndRoom:
                {
                    //place end room tile
                    PlaceEndRoom(EndTile);
                    this.gameObject.name = "Level " + PlacedTiles.Count; //debug so that we can tell how many "rooms" are in the level
                    break;
                }
            case Buildstate.PlaceCaps:
                {
                    FillOpenConnectors();
                    break;
                }
            case Buildstate.FillItems:
                {
                    //TODO: Fill rooms with objects 
                    PlaceRoomObjects();
                    break;
                }
            case Buildstate.CheckLevel:
                {
                    FinalizeLevel();
                    break;
                }
        }


    }

    //Loads resources, sets up lists and variables for the start of the level build process
    void Initialize()
    {
        //Initialize the lists
        //HallPrefabs = new List<Tile>();  //this needs to be init after we start loading from disk
        //RoomPrefabs = new List<Tile>(); //this needs to be init after we start loading from disk
        OpenConnectors = new List<Connector>();
        PlacedTiles = new List<Tile>();
        OpenItemMounts = new List<ItemMount>();

        failures = 0;
        iteration = 0;

        //TODO: load config settings from disk

        //TODO: load prefabs from disk
    }

    //Generates the level start, and processes random selections
    void PreGenerateLevel()
    {

    }

    //steps through each update cycle, and adds one tile to the level per cycle
    void ProcessLevelStep()
    {
        iteration++;

        //check to see if there is a tile in progress
        //if not, skip this step
        if (tileInProcess != null)
        {
            //check for collisions of previous placed tile
            //if no collisions were detected, finalize tile placement (by removing the used connectors)
            if (!CheckForCollision())
            {
                PlaceRoomInLevel();
            }
            else //if we collided, delete the prev placed tile and bump our failures up by 1
            {
                Destroy(tileInProcess.gameObject);

                targetConnector = null;
                processingConnector = null;
                tileInProcess = null;

                failures++;
            }
        }

        //if max number of tiles has not been reached, begin tile placement process
        if (PlacedTiles.Count >= Target_Number_Rooms)
        {
            buildState = Buildstate.PlaceEndRoom;
            return;
        }

        //Select a connector from the Open Connectors list
        targetConnector = GetOpenConnector();

        //Cycle through the prefabs, to see which ones we can use to randomly select a new tile from
        List<int> matchingTiles = new List<int>();
        for (int t = 0; t < TilePrefabs.Count; t++)
        {
            //TODO: check to see if this type of tile can connect to TilePrefabs[t].type

            //check connectors to see if we have at least one matching
            for (int c = 0; c < TilePrefabs[t].Connectors.Count; c++)
            {
                if (TilePrefabs[t].Connectors[c].Type != targetConnector.Type) //if the connector type doesn't match, continue searching
                    continue;

                matchingTiles.Add(t); //we match, so add this tile to the matching tiles list
                break; //and break, since we dont want to add the tile twice, in case it has more connectors that match
            }
        }

        //check and make sure we have a placeable tile
        if (matchingTiles.Count <= 0)
        {
            failures++;
            return;
        }

        //randomly select a tile from the matchingTiles list to try and place in the level, 
        //instantiate the new tile, and place it in our level so that the next pass can check for collisions
        PrePlaceRoom(TilePrefabs[matchingTiles[Random.Range(0, matchingTiles.Count)]]);
    }

    void FinalizeLevel()
    {
        //TODO: durring finalization of the level, should remove hallways that deadend without having a room attached. 
        //      every hallway should either loop back into the level, or should end in a room. No Hallway connected
        //      to a hallway and a dead end should exist. will have to be a loop of while that exists, do the loop again

        buildState = Buildstate.Done;
    }

    //returns true if there is a collision, false if there is no collision
    bool CheckForCollision()
    {
        Collider[] inProgressColliders = tileInProcess.GetComponentsInChildren<Collider>();

        for (int j = 0; j < inProgressColliders.Length; j++) //for all the colliders in the one to test
        {
            for (int i = 0; i < PlacedTiles.Count; i++) //and for all the tiles currently in the level
            {
                Collider[] checkColliders = PlacedTiles[i].GetComponentsInChildren<Collider>(); //get all the colliders for the tile in the level

                for (int k = 0; k < checkColliders.Length; k++) //and check all its colliders
                {
                    Bounds bounds1 = inProgressColliders[j].bounds;
                    Bounds bounds2 = checkColliders[k].bounds;
                    bounds1.Expand(-0.01f);
                    bounds2.Expand(-0.01f);
                    //test the currently selected colliders for collision
                    if (bounds1.Intersects(bounds2))
                    {
                        //if this is true, we need to return true
                        //Debug.Log("Collision Detected " + tileInProcess.name + ", " + PlacedTiles[i].name);
                        return true;
                    }
                }


            }
        }
        //if we get through all those tests, and we still haven't collided, then return no collisions with false
        return false;
    }

    void FillOpenConnectors()
    {
        //create a list of endcaps, and put them in a random order in a list
        List<Tile> EndCaps = new List<Tile>();
        for (int t = 0; t < TilePrefabs.Count; t++)
        {
            if (TilePrefabs[t].Type == TileType.EndCap)
                EndCaps.Insert(Random.Range(0, EndCaps.Count), TilePrefabs[t]);
        }

        Tile endCap = EndCaps[Random.Range(0, EndCaps.Count)]; //have to assign it to keep compiler from complaining

        for (int i = OpenConnectors.Count - 1; i >= 0; i--)
        {
            bool searchingForEndcap = true;

            while(searchingForEndcap) //until we find an endcap that fits our open connector
            {
                endCap = EndCaps[Random.Range(0, EndCaps.Count)]; //get a random endcap

                if (endCap.Connectors[0].Type == OpenConnectors[i].Type)
                    searchingForEndcap = false; //found our endcap, now lets assign it. 
            }

            //now that we have an endcap that fits, place it
            PlaceEndCap(OpenConnectors[i], endCap);
        }

        buildState = Buildstate.FillItems;
    }

    void PlaceEndCap(Connector targetConnector, Tile endCap)
    {
        //Instantiate, name, and set parent to this level generator object
        tileInProcess = GameObject.Instantiate(endCap, Vector3.zero, Quaternion.identity);
        tileInProcess.gameObject.name = endCap.Name + " " + iteration;
        tileInProcess.gameObject.transform.SetParent(this.transform);

        processingConnector = tileInProcess.Connectors[0]; //because endcaps only have 1 connector

        //rotate the tile to match the targetConnector
        Vector3 targetDoorwayEuler = targetConnector.transform.eulerAngles;
        Vector3 roomDoorwayEuler = processingConnector.transform.eulerAngles;
        float deltaAngle = Mathf.DeltaAngle(roomDoorwayEuler.y, targetDoorwayEuler.y);
        Quaternion currentRoomTargetRotation = Quaternion.AngleAxis(deltaAngle, Vector3.up);
        tileInProcess.transform.rotation = currentRoomTargetRotation * Quaternion.Euler(0, 180, 0);

        //move the tile so that the connectors overlap
        tileInProcess.transform.position = targetConnector.transform.parent.parent.transform.position; //set the tile to the parent object's position, since its nested, its 2 parents up
        Vector3 correctiveTranslation = targetConnector.transform.position - processingConnector.transform.position; //get the correction
        tileInProcess.transform.position += correctiveTranslation; //now apply the correction. 

        //add the tile to the level
        PlacedTiles.Add(tileInProcess);

        //add connectors to list
        OpenConnectors.AddRange(tileInProcess.Connectors);

        //add room itemmounts to list
        OpenItemMounts.AddRange(tileInProcess.ItemMounts);

        //Remove the used connectors from our master list and destroy them so they don't end up in the playable level
        OpenConnectors.Remove(targetConnector);
        OpenConnectors.Remove(processingConnector);
        Destroy(targetConnector.gameObject);
        Destroy(processingConnector.gameObject);

        targetConnector = null;
        processingConnector = null;
        tileInProcess = null;
    }

    void PlaceStartingRoom(Tile tile)
    {
        //Instantiate, name, and set parent to this level generator object
        Tile startTile = GameObject.Instantiate(tile, Vector3.zero, Quaternion.identity);
        startTile.gameObject.transform.SetParent(this.transform);

        PlacedTiles.Add(startTile);

        //add connectors to list
        OpenConnectors.AddRange(startTile.Connectors);

        //add room itemmounts to list
        OpenItemMounts.AddRange(startTile.ItemMounts);

        buildState = Buildstate.PlaceTiles;
    }

    void PrePlaceRoom(Tile tilePrefab)
    {
        //Instantiate, name, and set parent to this level generator object
        tileInProcess = GameObject.Instantiate(tilePrefab, Vector3.zero, Quaternion.identity);
        tileInProcess.gameObject.name = tilePrefab.Name + " " + iteration;
        tileInProcess.gameObject.transform.SetParent(this.transform);

        //create a randomized list of connectors 
        List<int> randomConnectorIndexs = new List<int>();
        for (int c = 0; c < tileInProcess.Connectors.Count; c++)
        {
            if (tileInProcess.Connectors[c].Type == targetConnector.Type)
                randomConnectorIndexs.Insert(Random.Range(0, randomConnectorIndexs.Count), c);
        }

        //pick our random connector to place at the targetConnector
        int connector = randomConnectorIndexs[Random.Range(0, randomConnectorIndexs.Count)];
        processingConnector = tileInProcess.Connectors[connector];

        //rotate the tile to match the targetConnector
        Vector3 targetDoorwayEuler = targetConnector.transform.eulerAngles;
        Vector3 roomDoorwayEuler = processingConnector.transform.eulerAngles;
        float deltaAngle = Mathf.DeltaAngle(roomDoorwayEuler.y, targetDoorwayEuler.y);
        Quaternion currentRoomTargetRotation = Quaternion.AngleAxis(deltaAngle, Vector3.up);
        tileInProcess.transform.rotation = currentRoomTargetRotation * Quaternion.Euler(0, 180, 0);

        //move the tile so that the connectors overlap
        tileInProcess.transform.position = targetConnector.transform.parent.parent.transform.position; //set the tile to the parent object's position, since its nested, its 2 parents up
        Vector3 correctiveTranslation = targetConnector.transform.position - processingConnector.transform.position; //get the correction
        tileInProcess.transform.position += correctiveTranslation; //now apply the correction. 
    }

    //TODO: make more generic so that i can use it for endcaps, rooms and hallways, etc
    void PlaceRoomInLevel()
    {
        //add the tile to the level
        PlacedTiles.Add(tileInProcess);

        //add connectors to list
        OpenConnectors.AddRange(tileInProcess.Connectors);

        //add room itemmounts to list
        OpenItemMounts.AddRange(tileInProcess.ItemMounts);

        //Remove the used connectors from our master list and destroy them so they don't end up in the playable level
        OpenConnectors.Remove(targetConnector);
        OpenConnectors.Remove(processingConnector);
        Destroy(targetConnector.gameObject);
        Destroy(processingConnector.gameObject);

        //may need to remove this??

        targetConnector = null;
        processingConnector = null;
        tileInProcess = null;
    }

    void PlaceRoomObjects()
    {
        buildState = Buildstate.CheckLevel;
    }

    void PlaceEndRoom(Tile tile)
    {
        //TODO: what happens when this loop fails to find a connector that can fit the exit room? rebuild level from scratch?

        buildState = Buildstate.PlaceCaps;
    }

    Connector GetOpenConnector()
    {
        return OpenConnectors[Random.Range(0, OpenConnectors.Count)];
    }

    //TODO: make the rebuildLevel(), so that in case it isn't a valid level, we can rebuild it from scratch
    void RebuildLevel()
    {

    }
}
